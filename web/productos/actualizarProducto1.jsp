<-- <%@page import="Modelo.Producto"%>
<%@page import="Modelo.ProductoDAO"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <% String codigo = request.getParameter("cod");
           ProductoDAO productoDao = new ProductoDAO();
           Producto producto = productoDao.readProduct(codigo);
        %>
        <div>
            <form action="actualizarProducto.jsp" method="GET">
                <div class="form-group">
                    <label for="formGroupExampleInput">C�digo Producto</label>
                    <input type="text" class="form-control" readonly="readonly" id="cod" name="cod" value="<%= producto.getCodigo() %>">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Nuevo Precio Venta</label>
                    <input type="text" class="form-control" id="precioVenta" name="precioVenta" placeHolder="Nuevo Precio Venta">
                </div>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </form>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>