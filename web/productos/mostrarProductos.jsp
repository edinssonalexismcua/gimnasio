<%-- 
    Document   : MostrarProductos
    Created on : 23/05/2020, 01:03:49 PM
    Author     : EDINSSON
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.ProductoDAO"%>
<%@page import="Modelo.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrar Productos</title>
    </head>
    <body>
        <ul class="nav nav-pills">
                <a class="nav-link active" href="agregarProducto.html">Agregar</a>
        </ul>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Codigo</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio Venta</th>
                    <th scope="col">Precio Compra</th>
                    <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <%  ProductoDAO productoDao = new ProductoDAO();
                    ArrayList<Producto> productos = productoDao.read();
                    for (Producto p : productos) {%>
                <tr>
                    <th scope="row"> <%=p.getCodigo()%> </th>
                    <td> <%=p.getNombre()%> </td>
                    <td> <%=p.getValorVenta()%></td>
                    <td> <%=p.getValorCompra()%></td>
                    <td>
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link active" href="actualizarProducto1.jsp?cod=<%= p.getCodigo() %>">Editar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="eliminarProducto.jsp?cod=<%= p.getCodigo()%>">Eliminar</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <% }%>
            </tbody>
        </table> 

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
