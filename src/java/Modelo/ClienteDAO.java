/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author EDINSSON
 */
public class ClienteDAO {
    public void create(Cliente cliente){
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        
       try{
           PreparedStatement statement = connection.prepareStatement("INSERT INTO clientes VALUES (?,?,?,?,?,?,?,?)");
           statement.setString(1, cliente.getDni());
           statement.setString(2, cliente.getNombre());
           statement.setString(3, cliente.getApellido());
           statement.setInt(4, cliente.getAltura());
           statement.setFloat(5, cliente.getPeso());
           statement.setInt(6, cliente.getEdad());
           statement.setBoolean(7, cliente.isVip());
           statement.setString(8, cliente.getDniEntrenador());
           statement.executeUpdate();
           connection.close();
       }catch(Exception e){
           e.printStackTrace();
       }
    }
    
    public ArrayList<Cliente> read(){
        ArrayList<Cliente> clientes = new ArrayList<>();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "SELECT * FROM clientes";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while(resultado.next()){
                clientes.add(new Cliente(resultado.getString("dni"), resultado.getString("nombre"), resultado.getString("apellido"), resultado.getInt("altura"), resultado.getFloat("peso"), resultado.getInt("edad"), resultado.getBoolean("vip")));
            }
            connection.close();
            return clientes;
            
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public void update(String dniNuevo, String dni){
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "UPDATE clientes SET dni = " + dniNuevo + " WHERE dni = " + dni;
            Statement sentencia = connection.createStatement();
            sentencia.executeUpdate(sql);
            
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void delete(Cliente c) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "DELETE FROM clientes WHERE dni = '" + c.getDni()+ "'";
            Statement sentencia = connection.createStatement();
            sentencia.executeUpdate(sql);
            
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
