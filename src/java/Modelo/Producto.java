/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author EDINSSON
 */
public class Producto {
    private String codigo;
    private String nombre;
    private double valorCompra;
    private double valorVenta;
    private String fechaIngreso;
    private int existencias;

    public Producto() {
    }

    public Producto(String codigo, String nombre, double valorCompra, double valorVenta, String fechaIngreso, int existencias) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.valorCompra = valorCompra;
        this.valorVenta = valorVenta;
        this.fechaIngreso = fechaIngreso;
        this.existencias = existencias;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public double getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(double valorVenta) {
        this.valorVenta = valorVenta;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }
    
    
}
