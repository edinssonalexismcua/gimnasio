/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author EDINSSON
 */
public class EmpleadoDAO {

    public void create(Empleado empleado) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO empleados VALUES (?,?,?,?,?)");
            statement.setString(1, empleado.getDni());
            statement.setString(2, empleado.getNombre());
            statement.setString(3, empleado.getApellido());
            statement.setInt(4, empleado.getEdad());
            statement.setString(5, empleado.getRol());
            statement.executeUpdate();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Empleado> read() {
        ArrayList<Empleado> Empleados = new ArrayList<>();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "SELECT * FROM empleados";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                Empleados.add(new Empleado(resultado.getString("dni"), resultado.getString("nombre"), resultado.getString("apellido"), resultado.getInt("edad"), resultado.getString("rol")));
            }
            connection.close();
            return Empleados;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(String dniNuevo, String dni) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "UPDATE empleaos SET dni = " + dniNuevo + " WHERE dni = " + dni;
            Statement sentencia = connection.createStatement();
            sentencia.executeUpdate(sql);
            
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void delete(Empleado p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "DELETE FROM empleados WHERE dni = '" + p.getDni() + "'";
            Statement sentencia = connection.createStatement();
            sentencia.executeUpdate(sql);

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
