/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author EDINSSON
 */
public class ProductoDAO {
    
    public void create(Producto producto){
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        
       try{
           PreparedStatement statement = connection.prepareStatement("INSERT INTO productos VALUES (?,?,?,?,?,?)");
           statement.setString(1, producto.getCodigo());
           statement.setString(2, producto.getNombre());
           statement.setDouble(3, producto.getValorCompra());
           statement.setDouble(4, producto.getValorVenta());
           statement.setString(5, producto.getFechaIngreso());
           statement.setInt(6, producto.getExistencias());
           statement.executeUpdate();
           connection.close();
       }catch(Exception e){
           e.printStackTrace();
       }
    }
    
    public ArrayList<Producto> read(){
        ArrayList<Producto> productos = new ArrayList<>();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "SELECT * FROM productos";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            
            while(resultado.next()){
                productos.add(new Producto(resultado.getString("codigo"), resultado.getString("nombre"), resultado.getDouble("valorCompra"), resultado.getDouble("valorVenta"), resultado.getString("fechaIngreso"), resultado.getInt("existencias")));
            }
            connection.close();
            return productos;
            
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public void update(double nuevo, String codigo){
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "UPDATE productos SET valorVenta = '" + nuevo + "' WHERE codigo = '" + codigo + "'";
            Statement sentencia = connection.createStatement();
            sentencia.executeUpdate(sql);
            
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void delete(String codigo) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "DELETE FROM productos WHERE codigo = '" + codigo + "'";
            Statement sentencia = connection.createStatement();
            sentencia.executeUpdate(sql);
            
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public Producto readProduct(String codigo){
        Producto producto = null;
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            String sql = "SELECT * FROM productos WHERE codigo='" + codigo +"'";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            
            while(resultado.next()){
                producto = new Producto(resultado.getString("codigo"), resultado.getString("nombre"), resultado.getDouble("valorCompra"), resultado.getDouble("valorVenta"), resultado.getString("fechaIngreso"), resultado.getInt("existencias"));
            }
            connection.close();
            return producto;
            
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
