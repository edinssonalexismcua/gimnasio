/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Demo;

import Modelo.Producto;
import Modelo.ProductoDAO;
import java.util.ArrayList;

/**
 *
 * @author Matias
 */
public class DemoDb {
    public static void main(String[] args) {
        ProductoDAO productoDao = new ProductoDAO();
        
        Producto p = productoDao.readProduct("1151901");
        
        System.out.println(p.getCodigo());
    }
}
